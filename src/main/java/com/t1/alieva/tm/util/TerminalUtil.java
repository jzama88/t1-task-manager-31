package com.t1.alieva.tm.util;

import com.t1.alieva.tm.exception.field.NumberIncorrectException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws NumberIncorrectException {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
