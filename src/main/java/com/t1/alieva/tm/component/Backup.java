package com.t1.alieva.tm.component;

import com.t1.alieva.tm.command.data.AbstractDataCommand;
import com.t1.alieva.tm.command.data.DataBackupLoadCommand;
import com.t1.alieva.tm.command.data.DataBackupSaveCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import java.nio.file.Files;
import java.nio.file.Paths;
import com.t1.alieva.tm.command.data.AbstractDataCommand;
import com.t1.alieva.tm.command.data.DataBackupLoadCommand;
import com.t1.alieva.tm.command.data.DataBackupSaveCommand;
import com.t1.alieva.tm.component.Bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start()  {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }


    public void stop() { es.shutdown(); }

    @SneakyThrows
    public void save()
    {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    @SneakyThrows
    public void load()
    {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }
}
