package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId) throws AbstractFieldException;


    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @NotNull String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    Task updateByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable
    Task changeTaskStatusByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @NotNull Status status) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable
    Task changeTaskStatusById(
            @NotNull String userId,
            @Nullable String id,
            @NotNull Status status) throws AbstractFieldException;
}
