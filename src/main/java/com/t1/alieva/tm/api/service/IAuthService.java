package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;

public interface IAuthService {

    @Nullable
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email) throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException;

    void login(@Nullable String login, @Nullable String password) throws
            AbstractFieldException,
            AbstractUserException,
            AbstractEntityNotFoundException,
            AuthenticationException;

    void logout();

    Boolean isAuth();

    @NotNull
    String getUserId() throws AbstractUserException;

    @NotNull
    User getUser() throws AbstractUserException, AbstractFieldException;

    void checkRoles(@Nullable Role[] roles) throws
            AbstractUserException,
            AbstractFieldException;
}
