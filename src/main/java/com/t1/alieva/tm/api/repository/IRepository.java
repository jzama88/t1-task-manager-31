package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.AbstractModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M add(@NotNull M model) throws
            AbstractEntityNotFoundException;

    @NotNull
    Collection<M> add (@NotNull Collection<M> models);

    @NotNull
    Collection<M> set (@NotNull Collection<M> models);

    void removeAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id) throws
            AbstractFieldException;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws
            AbstractFieldException;

    @Nullable
    M removeOne(@Nullable M model) throws
            AbstractEntityNotFoundException,
            AbstractFieldException;

    @Nullable
    M removeOneById(@Nullable String id) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @Nullable
    M removeOneByIndex(@Nullable Integer index) throws
            AbstractFieldException;

    int getSize();

    boolean existsById(@Nullable String id);

    void removeAll(@NotNull Collection<M> models);

}
