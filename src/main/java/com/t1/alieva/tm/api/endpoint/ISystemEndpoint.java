package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.ServerAboutRequest;
import com.t1.alieva.tm.dto.request.ServerVersionRequest;
import com.t1.alieva.tm.dto.response.ServerAboutResponse;
import com.t1.alieva.tm.dto.response.ServerVersionResponse;
import org.jetbrains.annotations.NotNull;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
