package com.t1.alieva.tm.api.endpoint;


import com.t1.alieva.tm.dto.request.AbstractRequest;
import com.t1.alieva.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation <RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}

