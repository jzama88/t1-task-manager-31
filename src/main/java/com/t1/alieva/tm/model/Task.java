package com.t1.alieva.tm.model;

import com.t1.alieva.tm.api.model.IWBS;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private Date created = new Date();

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @Nullable
    private String projectId;

    @NotNull
    private Status status = Status.NOT_STARTED;


    public Task() {

    }

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
