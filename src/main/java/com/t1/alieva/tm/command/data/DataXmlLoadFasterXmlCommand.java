package com.t1.alieva.tm.command.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.t1.alieva.tm.dto.Domain;
import com.t1.alieva.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand{

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    public static final String NAME = "data-load-xml-faster";

    @SneakyThrows
    @Override
    public void execute() throws JsonProcessingException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }


}

