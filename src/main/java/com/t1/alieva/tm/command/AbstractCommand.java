package com.t1.alieva.tm.command;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IServiceLocator;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;
import java.io.IOException;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    public abstract void execute() throws
            AuthenticationException,
            IOException,
            ClassNotFoundException,
            AbstractException;

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    public String getUserId() throws AbstractUserException {
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }
}
