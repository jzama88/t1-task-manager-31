package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.Domain;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.AuthenticationException;
import java.io.*;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load Data from Binary file";

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException,
            AuthenticationException,
            IOException,
            ClassNotFoundException {
        System.out.println("DATA BINARY LOAD");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
