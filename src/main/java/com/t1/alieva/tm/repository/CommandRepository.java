package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        @NotNull final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @NotNull final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Override
    @Nullable
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public @NotNull Collection<AbstractCommand> getCommandsWithArgument() {
        return mapByArgument.values();
    }
}
