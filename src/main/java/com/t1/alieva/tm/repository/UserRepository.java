package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findOneByLogin(@NotNull String login) {
        return models
                .stream()
                .filter(r -> login.equals(r.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findOneByEmail(@NotNull String email) {
        return models
                .stream()
                .filter(r -> email.equals(r.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(r -> login.equals(r.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(r -> email.equals(r.getEmail()));
    }
}
