package com.t1.alieva.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    private Socket socket;

    public AbstractEndpoint(
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        this.host = host;
        this.port = port;
    }

    @NotNull
    protected Object call(@NotNull final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }
}