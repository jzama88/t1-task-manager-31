package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IPropertyService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.LoginEmptyException;
import com.t1.alieva.tm.exception.field.PasswordEmptyException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.exception.user.PermissionException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import javax.naming.AuthenticationException;
import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IPropertyService propertyService;

    private final IUserService userService;

    private String userId;

    public AuthService(
            final IPropertyService propertyService,
            final IUserService userService)
    {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @Nullable
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            final String email) throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(
            @Nullable final String login,
            @Nullable final String password) throws
            AbstractFieldException,
            AbstractEntityNotFoundException,
            AbstractUserException, AuthenticationException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        final boolean locked = user.getLocked() == null || user.getLocked();
        if (locked) throw new AuthenticationException();
        @Nullable String hash = HashUtil.salt(propertyService,password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public Boolean isAuth() {
        return userId != null;
    }

    @Override
    @NotNull
    public String getUserId() throws AbstractUserException {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    @NotNull
    public User getUser() throws
            AbstractUserException,
            AbstractFieldException {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) throws
            AbstractUserException,
            AbstractFieldException {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @NotNull final Role role = user.getRole();
        //if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
