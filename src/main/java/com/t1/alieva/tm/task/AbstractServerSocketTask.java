package com.t1.alieva.tm.task;

import com.t1.alieva.tm.component.Server;
import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server);
        this.socket = socket;
    }
}
