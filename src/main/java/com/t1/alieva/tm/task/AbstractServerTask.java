package com.t1.alieva.tm.task;

import com.t1.alieva.tm.component.Server;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
